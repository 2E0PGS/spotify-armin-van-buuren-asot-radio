# Spotify Armin Van Buuren ASOT Radio

A JSON array of all the albums on Spotify.

## About

The Spotify web had some issues when scrolling right to the bottom of a artists uploads if they have a huge list of uploads. 

Sometimes duplicates would appear or items wouldn't load.

This was a inconvenience why trying to listen to every track/album in order so I decided to use Spotify's API to get a list which is easier to handle.

## JSON

A few example `JSONPaths` I used on the API results.

```
$.items[*].external_urls[*] 
$.items[*]['name'] 
$.items[*]['name']
$.items[*]['name','external_urls']
```
